﻿public interface IEnemy 
{
    void Damage();
    void Death();
}
