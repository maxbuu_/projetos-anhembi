﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elven : MonoBehaviour, IEnemy
{
    public int health;

    public void Damage()
    {
        if (Random.Range(0, 100) < 50)
            health--;

        if (health <= 0)
            Death();
    }

    public void Death()
    {
        gameObject.SetActive(false);
    }
}
