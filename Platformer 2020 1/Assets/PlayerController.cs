﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D myRigidbody;
    public float speed;
    public float jumpSpeed;

    public Animator myAnimator;

    public Vector2 physicsSpherePosition;
    public float physicsSphereRadius;
    public LayerMask floorLayer;

    public GameObject swoosh;
    public GameObject rockPrefab;

    public Transform muzzle;
    public Vector2 rockForce;
    public GameObject snake;
    public GameObject poison;
    public Text healthText;

    bool isPaused;

    public AudioSource audioSource;

    [Header("Efeitos Sonoros")]
    public AudioClip[] attackSfx;
    public AudioClip jumpSfx;
    public AudioClip poisonSfx;

    public int level;

    private void Start()
    {
        level = PlayerPrefs.GetInt("Level");
        transform.position = new Vector3(PlayerPrefs.GetFloat("PosX"), PlayerPrefs.GetFloat("PosY"), PlayerPrefs.GetFloat("PosZ"));
    }

    void Update()
    {
        MoveCharacter();

        if (Input.GetKeyDown(KeyCode.Space))
            Jump();

        if (Input.GetKeyDown(KeyCode.P))
            StartAttack();

        if (Input.GetKeyDown(KeyCode.O))
            ThrowRock();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;

            if (!isPaused)
                Time.timeScale = 0;
            else
                Time.timeScale = 1;
        }
    }

    void StartAttack()
    {
        myAnimator.Play("Attack");
    }

    public void Attack()
    {
        //audioSource.pitch = Random.Range(0.8f, 1.2f);
        level++;
        PlayerPrefs.SetInt("Level", level);
        PlayerPrefs.SetFloat("PosX", transform.position.x);
        PlayerPrefs.SetFloat("PosY", transform.position.y);
        PlayerPrefs.SetFloat("PosZ", transform.position.z);
        audioSource.PlayOneShot(attackSfx[Random.Range(0, attackSfx.Length)]);
        swoosh.SetActive(true);
    }

    public void ThrowRock()
    {
        GameObject rock = Instantiate(rockPrefab, muzzle.position, muzzle.rotation);
        rock.GetComponent<Rigidbody2D>().AddForce(transform.TransformDirection(rockForce), ForceMode2D.Impulse);

        Destroy(rock, 3);
    }

    void MoveCharacter()
    {
        float x = Input.GetAxis("Horizontal");
        myRigidbody.velocity = new Vector2(x * speed, myRigidbody.velocity.y);
        myAnimator.SetFloat("MoveSpeed", Mathf.Abs(x));        

        if (x > 0.05f)
            transform.rotation = Quaternion.Euler(0, 0, 0);
        else if (x < -0.05f)
            transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    public void Jump()
    {
        //myRigidbody.AddForce(new Vector2(0, jumpSpeed));
        if (IsOnGround())
        {
            audioSource.PlayOneShot(jumpSfx);
            myRigidbody.AddForce(Vector2.up * jumpSpeed, ForceMode2D.Impulse);
            myAnimator.SetTrigger("Jump");
        }
    }

    bool IsOnGround()
    {
        Vector2 physicsSphereOffset = physicsSpherePosition;

        if (transform.rotation.eulerAngles.y > 0)
            physicsSphereOffset = new Vector2(physicsSpherePosition.x * -1, physicsSpherePosition.y);

       return Physics2D.OverlapCircle((Vector2)transform.position + physicsSphereOffset, physicsSphereRadius, floorLayer);
    }

    private void OnDrawGizmos()
    {
        Vector2 physicsSphereOffset = physicsSpherePosition;

        if (transform.rotation.eulerAngles.y > 0)
            physicsSphereOffset = new Vector2(physicsSpherePosition.x * -1, physicsSpherePosition.y);

        Gizmos.DrawWireSphere((Vector2)transform.position + physicsSphereOffset, physicsSphereRadius);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (collision.GetComponent<Snake>() != null)
                StartPoison();
        }
    }

    public void StartPoison()
    {
        poison.SetActive(true);
        healthText.text = "Poison";
        audioSource.PlayOneShot(poisonSfx);
    }
}
