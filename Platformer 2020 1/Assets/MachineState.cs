﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineState : MonoBehaviour
{
    public enum States { IDLE, PATROL, ATTACK, RETREATING };
    public States currentState;

    void Start()
    {

    }

    private void Update()
    {
        switch(currentState)
        {
            case States.ATTACK:
                AttackState();
                break;
            case States.IDLE:
                IdleState();
                break;
            case States.PATROL:
                PatrolState();
                break;
            case States.RETREATING:
                RetreatingState();
                break;
        }
    }

    void AttackState()
    {
        // Executar ataque
        // Checar a distancia
        // if (distancia > xxxx)
        currentState = States.PATROL;
    }

    void IdleState()
    {

    }

    void PatrolState()
    {
        //if (distancia < xxxxx)
        currentState = States.ATTACK;
    }

    void RetreatingState()
    {
        //if (health < 10 && enemyCount <= 0)
        //{
        //    currentState = States.Homing;
        //    return;
        //}

        //if (isOutgunned)
        //{
        //    currentState = States.Defending;
        //    return;
        //}

        //transform.Translate(Vector3.right);
    }
}
