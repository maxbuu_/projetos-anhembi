﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour, IEnemy
{
    public int health = 3;

    public void Damage()
    {
        health--;

        if (health <= 0)
            Death();
    }

    public void Death()
    {
        gameObject.SetActive(false);
    }
}
