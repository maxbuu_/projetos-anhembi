﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ScoreSystem : MonoBehaviour
{
    public Knife knife;
    public Text scoreText;
    int currentScore;
    int bestScore;

    void Start()
    {
        knife.onKnifeHit += OnScoreChanged;
        //knife.onKnifeHit.AddListener(OnScoreChanged);
        Load();
        scoreText.text = "Score: " + currentScore + "\nBest Score:" + bestScore;
    }

    public void OnScoreChanged()
    {
        currentScore++;
        scoreText.text = "Score: " + currentScore + "\nBest Score:" + bestScore;
        Save();
    }

    void Save()
    {
        //PLAYER PREFS

        //if (currentScore > PlayerPrefs.GetInt("BestScore"))
        //PlayerPrefs.SetInt("BestScore", currentScore);


        //BINARIO
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/flippysave.save");
        Debug.Log(Application.persistentDataPath + "/flippysave.save");

        SaveData save = new SaveData();
        save.bestScore = currentScore;
        save.lastPositionX = knife.transform.position.x;
        save.lastPositionY = knife.transform.position.y;
        save.lastPositionZ = knife.transform.position.z;

        binaryFormatter.Serialize(file, save);
        file.Close();

        //JSON
        //string saveJson = JsonUtility.ToJson(save);
        //Debug.Log(saveJson);
    }

    void Load()
    {
        //PlayerPrefs
        //bestScore = PlayerPrefs.GetInt("BestScore");

        //BINARIO
        if(File.Exists(Application.persistentDataPath + "/flippysave.save"))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/flippysave.save", FileMode.Open);
            SaveData save = (SaveData)binaryFormatter.Deserialize(file);
            file.Close();

            bestScore = save.bestScore;
            //knife.transform.position = new Vector3(save.lastPositionX, save.lastPositionY, save.lastPositionZ);
        }

        //JSON
        //SaveData saveJson = JsonUtility.FromJson();
    }
}


[System.Serializable]
public class SaveData
{
    public int bestScore;
    public float lastPositionX;
    public float lastPositionY;
    public float lastPositionZ;
}
