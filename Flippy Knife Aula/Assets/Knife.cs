﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System;

public class Knife : MonoBehaviour
{
    private Rigidbody myRigidbody;

    public Vector3 startTouchPosition;
    public Vector3 endTouchPosition;

    public float launchForce;
    public Vector3 rotationForce;
    public Camera gameCamera;

    float timeInAir;
    bool isInAir = true;

    public UnityEvent onDeath;
    
    //public Action onKnifeHit;

    public delegate void OnKnifeHit();
    public OnKnifeHit onKnifeHit;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startTouchPosition = gameCamera.ScreenToViewportPoint(Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(0))
        {
            endTouchPosition = gameCamera.ScreenToViewportPoint(Input.mousePosition);
            LaunchKnife();
        }

        if (isInAir)
        {
            timeInAir += Time.deltaTime;
        }
    }

    void LaunchKnife()
    {
        timeInAir = 0;
        isInAir = true;
        myRigidbody.isKinematic = false;
        myRigidbody.AddForce((endTouchPosition - startTouchPosition) * launchForce);
        myRigidbody.AddTorque(rotationForce);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        onDeath.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("WoodenBlock") && timeInAir > 0.1f && !myRigidbody.isKinematic)
        {
            myRigidbody.isKinematic = true;
            isInAir = false;
            onKnifeHit?.Invoke();
            Debug.Log("Trigger " + other.name);
        }
    }
}
